﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balance : MonoBehaviour {
    public static Balance instance;

    private float amount = 100;
    [SerializeField] private List<Expense> expenses;

    private void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            instance = this;
        }
    }

    private void Start() {
        GameUI.instance.UpdateBalance(amount, false, 0, false);
    }

    public void AddToBalance(float value) {
        Debug.Log("Valor = " + value);
        amount += value;
        GameUI.instance.UpdateBalance(amount, false, value, true);
    }

    public void EndBalance() {
        float previousAmount = amount;
        int randomPercentage = Random.Range(0, 100);
        List<Expense> currentExpenses = expenses.FindAll(x => x.PercentageRate >= randomPercentage);

        foreach (Expense expense in currentExpenses) {
            float minValue = expense.MinValue;
            float maxValue = expense.MaxValue;
            float currentValue = Random.Range(minValue, maxValue);
            expense.CurrentValue = (float)System.Math.Round((decimal)currentValue, 2);
            amount -= expense.CurrentValue;
        }

        Player.instance.StopPlayer(amount);
        GameUI.instance.ActivateDayBalance(previousAmount, amount, currentExpenses);
    }
}