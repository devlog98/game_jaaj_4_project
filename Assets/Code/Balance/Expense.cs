﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Expense {
    [SerializeField] private string title;
    public string Title { get { return title; } }

    [SerializeField] private float minValue;
    public float MinValue { get { return minValue; } }
    [SerializeField] private float maxValue;
    public float MaxValue { get { return maxValue; } }

    [SerializeField] [Range(0, 100)] private int percentageRate;
    public int PercentageRate { get { return percentageRate; } }

    private float currentValue;
    public float CurrentValue { get { return currentValue; } set { currentValue = value; } }
}