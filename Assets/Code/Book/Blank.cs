﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blank {
    private string code;
    private string word;

    public string Code { get { return code; } }
    public string Word { get { return word; } }

    public Blank(string _code, string _word) {
        code = _code;
        word = _word;
    }
}