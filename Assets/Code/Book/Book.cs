﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class Book : MonoBehaviour {
    [Header("Story")]
    [SerializeField] private List<Page> pages;

    [Header("Minigames")]
    [SerializeField] private Insight insight;
    [SerializeField] private int insightSize = 5;

    private Page currentPage;
    private Queue<Page> pagesQueue = new Queue<Page>();
    private List<Blank> blanks = new List<Blank>();
    private bool finished;

    private void Start() {
        insight.Book = this;
        foreach (Page page in pages) {
            pagesQueue.Enqueue(page);
        }
        currentPage = pagesQueue.Dequeue();
        ComputerUI.instance.UpdateBook(currentPage.ParagraphKey, false);
    }

    public void CreatePage() {
        string paragraph = FillParagraphBlanks(currentPage.ParagraphKey);
        ComputerUI.instance.UpdateBook(paragraph, true);
    }

    public void NextPage(TMP_InputField currentInput) {
        if (!Player.instance.ChangedScreen) {
            if (!String.IsNullOrEmpty(currentInput.text)) {
                Blank newBlank = new Blank(currentPage.BlankCode, currentInput.text);
                blanks.Add(newBlank);

                if (pagesQueue.Count > 0) {
                    currentPage = pagesQueue.Dequeue();
                }
                else if (!finished) {                    
                    PDFGenerator.instance.GeneratePDF(pages, blanks);
                    finished = true;
                    return;
                }
                CreatePage();
                //insight.CreateInsight(insightSize);
            }
        }
    }

    private string FillParagraphBlanks(string paragraph) {
        foreach(Blank blank in blanks) {
            paragraph = paragraph.Replace(blank.Code, blank.Word);
        }
        return paragraph;
    }
}