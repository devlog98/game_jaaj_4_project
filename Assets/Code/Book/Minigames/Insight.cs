﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class Insight : MonoBehaviour {
    const string insightKeys = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private Book book;
    private List<char> ideas;
    public Book Book { get { return book; } set { book = value; } }

    public void CreateInsight(int size) {
        ideas = GenerateInsight(size);
        ComputerUI.instance.CreateInsight(ideas);
    }

    public void CheckInput(TMP_InputField currentInput) {
        if (!Player.instance.ChangedScreen) {
            if (!String.IsNullOrEmpty(currentInput.text)) {
                foreach (char idea in ideas) {
                    if (idea.ToString() == currentInput.text.ToUpper()) {
                        ComputerUI.instance.UpdateInsight(idea);
                        ideas.Remove(idea);
                        break;
                    }
                }

                if (ideas.Count == 0) {
                    book.CreatePage();
                }

                currentInput.text = "";
            }
        }
    }

    private List<char> GenerateInsight(int insightSize) {
        List<char> newIdeas = new List<char>();
        for (int i = 0; i < insightSize; i++) {
            char idea = insightKeys[UnityEngine.Random.Range(0, insightKeys.Length)];
            newIdeas.Add(idea);
        }
        return newIdeas;
    }
}