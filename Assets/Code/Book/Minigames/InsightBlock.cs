﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsightBlock {
    private string code;
    public string Code { get { return code; } }

    public InsightBlock(string _code) {
        code = _code;
    }
}