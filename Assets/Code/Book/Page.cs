﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Page {
    [SerializeField] private string paragraphKey;
    public string ParagraphKey { get { return paragraphKey; } }
    [SerializeField] private string blankCode;
    public string BlankCode { get { return blankCode; } }
}