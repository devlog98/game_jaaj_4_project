﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clock : MonoBehaviour {
    private const float BASESECONDS = 480;

    [Range(0, 23)] [SerializeField] private int dayStartHour;
    [Range(0, 23)] [SerializeField] private int dayEndHour;
    [SerializeField] private float dayDurationInSeconds;

    private int dayStart;
    private int dayEnd;
    private float second;

    private float time;
    private int clockTime; 
    private bool active;

    private void Start() {
        StartCoroutine("Timer");
    }

    private IEnumerator Timer() {
        dayStart = dayStartHour * 60;
        dayEnd = dayEndHour * 60;
        second = (dayDurationInSeconds / BASESECONDS);

        clockTime = dayStart;

        active = true;
        while (active) {
            if (Time.time > time + second) {
                time = Time.time;
                clockTime++;
                ComputerUI.instance.UpdateClock(clockTime);

                if (clockTime == dayEnd) {
                    active = false;
                    Balance.instance.EndBalance();
                }
            }

            yield return null;
        }
    }
}