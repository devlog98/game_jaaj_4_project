﻿using UnityEngine;
using System.Collections;
using System.IO;
using sharpPDF;
using sharpPDF.Enumerators;
using SimpleFileBrowser;
using System.Collections.Generic;

public class PDFGenerator : MonoBehaviour {
    public static PDFGenerator instance;

    internal string attacName;
    List<string> paragraphs = new List<string>();
    List<Page> pages;
    List<Blank> blanks;

    private void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            instance = this;
        }
    }

    public void GeneratePDF(List<Page> _pages, List<Blank> _blanks) {
        pages = _pages;
        blanks = _blanks;

        foreach (Page page in pages) {
            paragraphs.Add(FillParagraphBlanks(page.ParagraphKey, page.BlankCode));
        }

        StartCoroutine(CreatePDF());
    }

    private IEnumerator CreatePDF() {
        FileBrowser.SetFilters(true, new FileBrowser.Filter("Text Files", ".pdf"));
        FileBrowser.SetDefaultFilter(".pdf");
        FileBrowser.SetExcludedExtensions(".lnk", ".tmp", ".zip", ".rar", ".exe");
        FileBrowser.AddQuickLink("Users", "C:\\Users", null);

        yield return FileBrowser.WaitForSaveDialog(false, null, "myBook", "Save");

        if (FileBrowser.Success) {
            attacName = FileBrowser.Result;
            pdfDocument myDoc = new pdfDocument("Sample Application", "Me", false);
            pdfPage myPage = myDoc.addPage();

            int index = 0;
            foreach (string paragraph in paragraphs) {
                List<string> chunks = GetChunks(paragraph, 80);
                foreach (string chunk in chunks) {
                    myPage.addText(chunk, 15, 750 - (20 * index), predefinedFont.csHelveticaOblique, 15, new pdfColor(predefinedColor.csBlack));
                    index++;

                    if (index % 36 == 0) {
                        myPage = myDoc.addPage();
                        index = 0;
                    }
                }
            }

            myDoc.createPDF(attacName);
        }
    }

    private string FillParagraphBlanks(string paragraph, string blankCode) {
        foreach (Blank blank in blanks) {
            paragraph = paragraph.Replace(blank.Code, blank.Word);
        }
        paragraph = paragraph.Replace("_____", (blanks.Find(x => x.Code == blankCode).Word));
        return paragraph;
    }

    private List<string> GetChunks(string value, int chunkSize) {
        // The new result, will be turned into string[]
        var newSplit = new List<string>();
        // Split on normal chars, ie newline, space etc
        var splitted = value.Split();
        // Start out with null
        string word = null;

        for (int i = 0; i < splitted.Length; i++) {
            // If first word, add
            if (word == null) {
                word = splitted[i];
            }
            // If too long, add
            else if (splitted[i].Length + 1 + word.Length > chunkSize) {
                newSplit.Add(word);
                word = splitted[i];
            }
            // Else, concatenate and go again
            else {
                word += " " + splitted[i];
            }
        }
        // Flush what we have left, ie the last word
        newSplit.Add(word);

        // Convert into string[] (a requirement?)
        return newSplit;
    }
}