﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {
    public static Player instance;

    private bool changedScreen;
    public bool ChangedScreen { get { return changedScreen; } }
    private bool playing = true;
    private float currentAmount;
    private bool bookFinished;

    private void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            instance = this;
        }
    }

    private void Update() {
        if (playing) {
            if (Input.GetButtonDown("Jump")) {
                MenuUI.instance.NewGame();
            }

            if (Input.GetButtonDown("ChangeScreen")) {
                changedScreen = true;
                ComputerUI.instance.ChangeScreen();
            }
            else {
                changedScreen = false;
            }
        }
        else {
            if (Input.GetButtonDown("Jump")) {
                if (currentAmount < 0) {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }
                else {
                    playing = true;
                    GameObject.FindObjectOfType<Clock>().StartCoroutine("Timer");
                    GameUI.instance.DeactivateDayBalance();
                }
            }
        }

        if (Input.GetButtonDown("Cancel")) {
            Application.Quit();
        }
    }

    public void StopPlayer(float amount) {
        playing = false;
        currentAmount = amount;
    }
}