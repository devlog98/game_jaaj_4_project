﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class ComputerUI : MonoBehaviour {
    public static ComputerUI instance;

    [Header("Screens")]
    [SerializeField] GameObject workScreen;
    [SerializeField] GameObject bookScreen;
    [SerializeField] GameObject insightScreen;
    private List<GameObject> screens = new List<GameObject>();
    private GameObject currentScreen;
    private int screenIndex;

    [Header("Clock")]
    [SerializeField] private TextMeshProUGUI clockText;

    [Header("Work")]
    [SerializeField] private TextMeshProUGUI taskText;
    [SerializeField] private TMP_InputField taskInput;

    [Header("Book")]
    [SerializeField] private TextMeshProUGUI paragraphText;
    [SerializeField] private TMP_InputField blankInput;

    [Header("Insight")]
    [SerializeField] private GameObject insightBlockText;
    [SerializeField] private int insightBlockXDistance;
    [SerializeField] private int insightBlockYDistance;
    [SerializeField] private TMP_InputField insightInput;
    private List<GameObject> insightObjects = new List<GameObject>();

    private void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            instance = this;
        }
    }

    private void Start() {
        screens.Add(workScreen);
        screens.Add(bookScreen);
        foreach (GameObject screen in screens) {
            DeactivateScreen(screen);
        }
        currentScreen = screens[screenIndex];
        ActivateScreen(currentScreen);
    }

    public void ChangeScreen() {
        screenIndex++;
        if (screenIndex == screens.Count) {
            screenIndex = 0;
        }

        DeactivateScreen(currentScreen);
        currentScreen = screens[screenIndex];
        ActivateScreen(currentScreen);
    }

    private void ActivateScreen(GameObject screen) {
        screen.SetActive(true);
        EventSystem.current.SetSelectedGameObject(screen);
    }

    private void DeactivateScreen(GameObject screen) {
        screen.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);
    }

    public void UpdateClock(int time) {
        float minutes = Mathf.Floor(time / 60);
        float seconds = Mathf.RoundToInt(time % 60);
        string niceTime = string.Format("{0:00}:{1:00}", minutes, seconds);
        clockText.text = niceTime;
    }

    public void UpdateWork(string task, string input) {
        taskText.text = task;
        taskInput.text = input;
    }

    public void UpdateBook(string paragraph, bool swapScreen) {
        if (swapScreen) {
            SwapScreens(bookScreen);
        }
        paragraphText.text = paragraph;
        blankInput.text = "";
        DeactivateScreen(currentScreen);
        ActivateScreen(currentScreen);
    }

    public void CreateInsight(List<char> blocks) {
        SwapScreens(insightScreen);
        foreach (char block in blocks) {
            GameObject newBlock = Instantiate(insightBlockText, insightInput.gameObject.transform);

            RectTransform newBlockTransform = newBlock.GetComponent<RectTransform>();
            int newX = Random.Range(-insightBlockXDistance, insightBlockXDistance);
            int newY = Random.Range(-insightBlockYDistance, insightBlockYDistance);
            newBlockTransform.anchoredPosition = new Vector2(newX, newY);

            TextMeshProUGUI newBlockText = newBlock.GetComponent<TextMeshProUGUI>();
            newBlockText.text = block.ToString();
            newBlock.name = block.ToString();

            insightObjects.Add(newBlock);
        }
        insightObjects.Reverse();
    }

    public void UpdateInsight(char code) {
        foreach (GameObject insight in insightObjects) {
            if (insight.name == code.ToString()) {
                Animator anim = insight.GetComponent<Animator>();
                anim.SetTrigger("Activate");
                insightObjects.Remove(insight);
                Destroy(insight.gameObject, anim.GetCurrentAnimatorStateInfo(0).length);
                break;
            }
        }
    }

    private void SwapScreens(GameObject newScreen) {
        DeactivateScreen(currentScreen);
        screens.Remove(currentScreen);
        currentScreen = newScreen;
        screens.Add(currentScreen);
        ActivateScreen(currentScreen);
    }
}