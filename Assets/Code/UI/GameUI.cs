﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class GameUI : MonoBehaviour {
    public static GameUI instance;

    [Header("Balance")]
    [SerializeField] private TextMeshProUGUI amountText;
    [SerializeField] private TextMeshProUGUI addedValueText;
    [SerializeField] private Animator addedValueAnim;

    [Header("Day Balance")]
    [SerializeField] private GameObject dayBalance;
    [SerializeField] private Animator dayBalanceAnim;
    [SerializeField] private TextMeshProUGUI balanceText;
    [SerializeField] private TextMeshProUGUI previousBalanceText;
    [SerializeField] private GameObject expenseText;
    [SerializeField] private float expenseYDistance;
    private List<GameObject> expenseObjects = new List<GameObject>();

    private void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            instance = this;
        }
    }

    public void UpdateBalance(float amount, bool amountSign, float addedValue, bool addedValueSign) {
        amountText.text = NumberToCurrency(amount, amountSign);
        addedValueText.text = addedValue > 0 ? NumberToCurrency(addedValue, addedValueSign) : "";
        addedValueAnim.SetTrigger("Activate");
        Debug.Log(amount);
        Debug.Log(amountSign);
        Debug.Log(addedValue);
        Debug.Log(addedValueSign);
    }

    public void ActivateDayBalance(float previousBalance, float currentBalance, List<Expense> expenses) {
        dayBalanceAnim.SetTrigger("Activate");

        if (currentBalance < 0) {
            balanceText.text = "ACABOU A GRANA E SEU SONHO....";
        }
        else {
            balanceText.text = "Restante: " + NumberToCurrency(currentBalance, false);
        }
        amountText.text = NumberToCurrency(currentBalance, false);

        int index = 0;
        RectTransform balanceTransform = balanceText.GetComponent<RectTransform>();
        foreach (Expense expense in expenses) {
            index++;

            GameObject newExpense = Instantiate(expenseText, dayBalance.transform);

            RectTransform newExpenseTransform = newExpense.GetComponent<RectTransform>();
            newExpenseTransform.anchoredPosition = new Vector2(0, balanceTransform.anchoredPosition.y + (expenseYDistance * index));

            TextMeshProUGUI newExpenseText = newExpense.GetComponent<TextMeshProUGUI>();
            newExpenseText.text = expense.Title + ": " + NumberToCurrency(-expense.CurrentValue, true);

            Animator newExpenseAnim = newExpense.GetComponent<Animator>();
            newExpenseAnim.SetTrigger("Activate");

            expenseObjects.Add(newExpense);
        }

        index++;
        previousBalanceText.text = "Renda no Dia: " + NumberToCurrency(previousBalance, false);
        RectTransform previousBalanceTransform = previousBalanceText.GetComponent<RectTransform>();
        previousBalanceTransform.anchoredPosition = new Vector2(0, balanceTransform.anchoredPosition.y + (expenseYDistance * index));
    }

    public void DeactivateDayBalance() {
        dayBalanceAnim.SetTrigger("Deactivate");
        foreach (GameObject expense in expenseObjects) {
            Animator anim = expense.GetComponent<Animator>();
            anim.SetTrigger("Deactivate");
            Destroy(expense.gameObject, anim.GetCurrentAnimatorStateInfo(0).length);
        }
        expenseObjects.Clear();
    }

    private string NumberToCurrency(float value, bool withSign) {
        if (withSign) {
            return (Mathf.Sign(value) > 0 ? "+" : "-") + "$" + Mathf.Abs(value).ToString("0.00");
        }
        else {
            return "$" + Mathf.Abs(value).ToString("0.00");
        }
    }
}