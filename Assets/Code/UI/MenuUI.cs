﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;

public class MenuUI : MonoBehaviour {
    public static MenuUI instance;

    [SerializeField] private Animator menuAnim;
    [EventRef] public string music;
    EventInstance musicEvent;

    private void Awake() {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            musicEvent = RuntimeManager.CreateInstance(music);
        }


    }

    private void Start() {
       
        musicEvent.start();
    }

    public void NewGame() {
        menuAnim.SetTrigger("Activate");
        musicEvent.setParameterByName("Parameter 1", 1);
    }
}
