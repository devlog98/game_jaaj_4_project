﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class Work : MonoBehaviour {
    private string task;
    private string taskInput;

    const string taskKeys = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!?@#$%&*";

    private void Start() {
        CreateTask();
    }

    private void CreateTask() {
        task = GenerateTask(10);
        taskInput = "";
        ComputerUI.instance.UpdateWork(task, taskInput);
    }

    public void CheckInput(TMP_InputField currentInput) {
        if (!String.IsNullOrEmpty(currentInput.text)) {
            Debug.Log(currentInput.text);
            char[] inputChars = currentInput.text.ToUpper().ToCharArray();
            char lastInputChar = inputChars[inputChars.Length - 1];
            char[] taskChars = task.ToCharArray();

            if (lastInputChar == taskChars[inputChars.Length - 1]) {
                taskInput = currentInput.text.ToUpper();

                if (taskInput == task) {
                    Balance.instance.AddToBalance(15);
                    task = GenerateTask(10);
                    taskInput = "";
                }
            }

            ComputerUI.instance.UpdateWork(task, taskInput);
        }
    }

    private string GenerateTask(int taskSize) {
        char[] taskChars = new char[taskSize];
        for(int i = 0; i < taskSize; i++) {
            taskChars[i] = taskKeys[UnityEngine.Random.Range(0, taskKeys.Length)];
        }
        return new String(taskChars);
    }
}